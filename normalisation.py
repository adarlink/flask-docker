import re
import unicodedata
import pandas as pd
import numpy as np
import contractions
import spacy
import nltk

from nltk.tokenize import TweetTokenizer
from bs4 import BeautifulSoup

from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer

tweettokenizer = TweetTokenizer()
nlp = spacy.load('en', parse = False, tag=False, entity=False)
stopword_list = nltk.corpus.stopwords.words('english')
stopword_list.remove('no')
stopword_list.remove('not')

#HTML decoding
def strip_html_tags(text):
    soup = BeautifulSoup(text, "lxml")
    text = soup.get_text()
    return text

#remove username, @mention
def remove_username(text):
    text = re.sub(r"@[^\s]+[\s]?", " ", text)
    return text

#remove propname
def remove_propname(text):
    text = re.sub(r"\*PROPNAME\*", "", text)
    return text

# remove url links
def remove_url(text):
    text = re.sub(r"http.?://[^\s]+[\s]?", " ", text)
    text = re.sub(r"www.[^\s]+[\s]?", " ", text)
    return text

#remove numbers
def remove_number(text):
    text = re.sub(r"\s?[0-9]+\.?[0-9]*", " ", text)
    return text

#remove twitpic
def remove_twitpic(text):
    text = re.sub(r"pic\.twitter\.com/[^\s]+[\s]?", " ", text)
    return text

#remove spacial chars including hashtag & punctuation
def remove_special_chars(text):
    text = re.sub('[^a-zA-z0-9\s]', " ", text)
    return text

#remove accented chars
def remove_accented_chars(text):
    text = unicodedata.normalize('NFKD', text).encode('ascii', 'ignore').decode('utf-8', 'ignore')
    return text


#contraction expansion
def contraction_expansion(text):
    text = contractions.fix(text)
    return text

#lemmatising
def lemmatize_text(text):
    text = nlp(text)
    text = ' '.join([word.lemma_ if word.lemma_ != '-PRON-' else word.text for word in text])
    return text

#stemming
def stem_text(text):
    text = text.split()
    stemmer = SnowballStemmer("english")
    stemmed_words = [stemmer.stem(word) for word in text]
    text = " ".join(stemmed_words)
    return text

#remove stopwords
def remove_stopwords(text):
    tokens = tweettokenizer.tokenize(text)
    tokens = [token.strip() for token in tokens]
    filtered_tokens = [token for token in tokens if token.lower() not in stopword_list]
    text = ' '.join(filtered_tokens)
    return text


#combine all fuctions
def normalise_corpus(corpus,
                     html_stripping=True,

                     propname_removal=True,
                     username_removal=True,
                     url_removal=True,
                     numeric_removal=True,
                     twitpic_link_removal=True,
                     special_char_removal=True,
                     accented_char_removal=True,

                     contraction_expansion=True,
                     text_lower_case=True,
                     text_lemmatization=True,
                     text_stemming=False,
                     stopword_removal=True):

    normalised_corpus = []
    for text in corpus:
        text = str(text)
        #remove url links
        if url_removal:
            text = remove_url(text)

        #HTML decoding
        if html_stripping:
            text = strip_html_tags(text)

        #remove username, mentions
        if username_removal:
            text = remove_username(text)

        #remove_propname
        if propname_removal:
            text = remove_propname(text)

        #remove numbers
        if  numeric_removal:
            text = remove_number(text)

        #remove twitpic links
        if  twitpic_link_removal:
            text = remove_twitpic(text)

        # remove accented characters
        if accented_char_removal:
            text = remove_accented_chars(text)

        # lowercase the text
        if text_lower_case:
            text = text.lower()

        # expand contractions
        #if contraction_expansion:
        #    doc = contraction_expansion(doc)
        text = contractions.fix(text)
        # remove extra newlines
        text = re.sub(r'[\r|\n|\r\n]+', " ",text)


        # lemmatise text
        if text_lemmatization:
            text = lemmatize_text(text)

        # stemming text
        if text_stemming:
            text = stem_text(text)

        # remove special characters
        if special_char_removal:
            text = remove_special_chars(text)

        # remove extra whitespace
        text = re.sub(' +', " ", text)

        # remove stopwords
        if stopword_removal:
            text = remove_stopwords(text)
        #normalised_corpus.append(doc)

        normalised_corpus.append(text)

    return normalised_corpus

def normalise_text(text,
                     html_stripping=True,

                     propname_removal=True,
                     username_removal=True,
                     url_removal=True,
                     numeric_removal=True,
                     twitpic_link_removal=True,
                     special_char_removal=True,
                     accented_char_removal=True,

                     contraction_expansion=True,
                     text_lower_case=True,
                     text_lemmatization=True,
                     text_stemming=False,
                     stopword_removal=True):

        text = str(text)

        #remove url links
        if url_removal:
            text = remove_url(text)

        #HTML decoding
        if html_stripping:
            text = strip_html_tags(text)

        #remove username, mentions
        if username_removal:
            text = remove_username(text)

        #remove_propname
        if propname_removal:
            text = remove_propname(text)


        #remove numbers
        if  numeric_removal:
            text = remove_number(text)

        #remove twitpic links
        if  twitpic_link_removal:
            text = remove_twitpic(text)

        # remove accented characters
        if accented_char_removal:
            text = remove_accented_chars(text)

        # lowercase the text
        if text_lower_case:
            text = text.lower()

        # expand contractions
        #if contraction_expansion:
        #    doc = contraction_expansion(doc)
        text = contractions.fix(text)
        # remove extra newlines
        text = re.sub(r'[\r|\n|\r\n]+', " ",text)


        # lemmatise text
        if text_lemmatization:
            text = lemmatize_text(text)

        # stemming text
        if text_stemming:
            text = stem_text(text)

        # remove special characters
        if special_char_removal:
            text = remove_special_chars(text)

        # remove extra whitespace
        text = re.sub(' +', " ", text)

        # remove stopwords
        if stopword_removal:
            text = remove_stopwords(text)
        #normalised_corpus.append(doc)

        return text
