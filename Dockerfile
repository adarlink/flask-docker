FROM ubuntu:latest
LABEL maintainer="Adi Darliansyah <adarliansyah@outlook.com>"
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential

EXPOSE 8081

COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt
RUN python -m nltk.downloader stopwords
RUN python -m nltk.downloader vader_lexicon
RUN python -m spacy download en
RUN chmod 644 /app

ENTRYPOINT ["python"]
CMD ["app.py"]
