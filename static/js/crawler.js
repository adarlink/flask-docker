$(function(){
	$('#btnCrawler').click(function(){

		$.ajax({
			url: '/crawler',
			data: $('form').serialize(),
			type: 'POST',
			success: function(response){
				console.log(response);
			},
			error: function(error){
				console.log(error);
			}
		});
	});
	$('#btnCleaner').click(function(){

		$.ajax({
			url: '/cleaner',
			data: $('form').serialize(),
			type: 'POST',
			success: function(response){
				console.log(response);
			},
			error: function(error){
				console.log(error);
			}
		});
	});
});
