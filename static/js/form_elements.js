

$(document).ready(function(){
	  //Dropdown menu - select2 plug-in
	   $("#lang-model").select2();

	   $("#source").select2();

	  $("#senti-class").select2();

	 
	  

	  //Multiselect - Select2 plug-in
	  $("#multi").val(["Jim","Lucy"]).select2();

	  //Date Pickers
	  $('.input-append.date').datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
				todayHighlight: true
	   });

	 $('#dp5').datepicker();

	 $('#sandbox-advance').datepicker({
			format: "dd/mm/yyyy",
			startView: 1,
			daysOfWeekDisabled: "3,4",
			autoclose: true,
			todayHighlight: true
    });

	//Time pickers
	$('.timepicker-default').timepicker();
    $('.timepicker-24').timepicker({
                minuteStep: 1,
                showSeconds: true,
                showMeridian: false
     });
	//Color pickers
	$('.my-colorpicker-control').colorpicker()

	//Input mask - Input helper
	$(function($){
	   $("#date").mask("99/99/9999");
	   $("#phone").mask("(999) 999-9999");
	   $("#tin").mask("99-9999999");
	   $("#ssn").mask("999-99-9999");
	});

	//Autonumeric plug-in - automatic addition of dollar signs,etc controlled by tag attributes
	$('.auto').autoNumeric('init');

	//HTML5 editor
	$('#text-editor').wysihtml5();

	//Drag n Drop up-loader
	$("div#myId").dropzone({ url: "/file/post" });

	// Moris Charts - Line Charts
	
	Morris.Line({
	  element: 'line-example',
	  data: [
		{ y: '2006', a: 50, b: 40 },
		{ y: '2007', a: 65,  b: 55 },
		{ y: '2008', a: 50,  b: 40 },
		{ y: '2009', a: 75,  b: 65 },
		{ y: '2010', a: 50,  b: 40 },
		{ y: '2011', a: 75,  b: 65 },
		{ y: '2012', a: 100, b: 90 }
	  ],
	  xkey: 'y',
	  ykeys: ['a', 'b'],
	  labels: ['Series A', 'Series B'],
	  lineColors:['#0aa699','#d1dade'],
	});
	
	Morris.Area({
	  element: 'area-example',
	  data: [
		{ y: '2006', a: 100, b: 90 },
		{ y: '2007', a: 75,  b: 65 },
		{ y: '2008', a: 50,  b: 40 },
		{ y: '2009', a: 75,  b: 65 },
		{ y: '2010', a: 50,  b: 40 },
		{ y: '2011', a: 75,  b: 65 },
		{ y: '2012', a: 100, b: 90 }
	  ],
	  xkey: 'y',
	  ykeys: ['a', 'b'],
	  labels: ['Series A', 'Series B'],
	  lineColors:['#0090d9','#b7c1c5'],
	  lineWidth:'0',
	   grid:'false',
	  fillOpacity:'0.5'
	});
	
	Morris.Donut({
	  element: 'donut-example',
	  data: [
		{label: "Download Sales", value: 12},
		{label: "In-Store Sales", value: 30},
		{label: "Mail-Order Sales", value: 20}
	  ],
	  colors:['#60bfb6','#91cdec','#eceff1']
	});


});
