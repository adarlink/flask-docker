#!/usr/local/bin/python3
import os
import nltk
import pandas as pd
import contractions
import spacy

from flask import Flask, render_template, json, request, redirect, url_for, send_from_directory, jsonify

#lexical resource
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from afinn import Afinn
from nltk.corpus import sentiwordnet as swn

#dependency
import normalisation
import scoring
import controllers
import models

from datetime import datetime
from werkzeug.utils import secure_filename

# Keras
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.models import model_from_json
from keras import backend as K
import re
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer

# declare constants
HOST = '0.0.0.0'
PORT = 8081

UPLOAD_FOLDER = 'files/'
TEMP_FOLDER = 'temp/'

ALLOWED_EXTENSIONS = set(['csv'])
ANALYZER = SentimentIntensityAnalyzer()
MAX_SEQUENCE_LENGTH = 100

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['TEMP_FOLDER'] = TEMP_FOLDER


#VADER
def vader_sentiment(normalised_text):
    upper_threshold=0.05
    lower_threshold=-0.05
    # get aggregate scores and final sentiment
    scores = ANALYZER.polarity_scores(normalised_text)
    # get aggregate scores and final sentiment
    agg_score = scores['compound']
    pos_score = scores['pos']
    neg_score = scores['neg']
    neu_score = scores['neu']

    if agg_score > upper_threshold:
        sentiment = 'positive'
    elif agg_score < lower_threshold:
        sentiment = 'negative'
    else:
        sentiment = 'neutral'

    return pd.Series([pos_score,neg_score,neu_score,agg_score,sentiment])

#VADER
def lexicon_vader(text):
    shortText = []

    upper_threshold=0.05
    lower_threshold=-0.05
    normalised_text = normalise_text(text)
    # get aggregate scores and final sentiment
    scores = ANALYZER.polarity_scores(normalised_text)
    # get aggregate scores and final sentiment
    agg_score = scores['compound']
    pos_score = scores['pos']
    neg_score = scores['neg']
    neu_score = scores['neu']

    if agg_score > upper_threshold:
        sentiment = 'positive'
    elif agg_score < lower_threshold:
        sentiment = 'negative'
    else:
        sentiment = 'neutral'

    return pd.Series([sentiment,normalised_text])

#AFINN
def afinn_sentiment(sentence):
    upper_threshold=1
    lower_threshold=-1
    afn = Afinn(emoticons=True)
    afn_score = afn.score(sentence)

    if afn_score >= upper_threshold:
         sentiment = 'positive'
    elif afn_score <= lower_threshold:
         sentiment = 'negative'
    else:
         sentiment = 'neutral'
    return pd.Series([afn_score,sentiment])

#SWN
def swn_sentiment(sentence):
    upper_threshold=0.01
    lower_threshold=-0.01

    nlp = spacy.load('en', parse = False, tag=False, entity=False)
    tagged_text = [(token.text, token.tag_) for token in nlp(sentence)]
    pos_score = neg_score = token_count = obj_score = 1

    for word, tag in tagged_text:
        ss_set = None
        if 'NN' in tag and list(swn.senti_synsets(word, 'n')):
            ss_set = list(swn.senti_synsets(word, 'n'))[0]
        elif 'VB' in tag and list(swn.senti_synsets(word, 'v')):
            ss_set = list(swn.senti_synsets(word, 'v'))[0]
        elif 'JJ' in tag and list(swn.senti_synsets(word, 'a')):
            ss_set = list(swn.senti_synsets(word, 'a'))[0]
        elif 'RB' in tag and list(swn.senti_synsets(word, 'r')):
            ss_set = list(swn.senti_synsets(word, 'r'))[0]
        # if senti-synset is found
        if ss_set:
            # add scores for all found synsets
            pos_score += ss_set.pos_score()
            neg_score += ss_set.neg_score()
            obj_score += ss_set.obj_score()
            token_count += 1

    # aggregate final scores
    final_score = pos_score - neg_score
    norm_final_score = round(float(final_score) / token_count, 2)

    if norm_final_score > upper_threshold:
         sentiment = 'positive'
    elif norm_final_score < lower_threshold:
         sentiment = 'negative'
    else:
         sentiment = 'neutral'

    return pd.Series([pos_score,neg_score,obj_score,sentiment])

#OCEAN ALL
def ocean(normalised_text, sentiment, lexicon, model):
    shortText = []

   #sequence padding
    MAX_NB_WORDS = 20000
    MAX_SEQUENCE_LENGTH = 100
    K.clear_session()
    tokenizer = Tokenizer(num_words= MAX_NB_WORDS)
    shortText.append(normalised_text)
    tokenizer.fit_on_texts(shortText)
    sequences = tokenizer.texts_to_sequences(shortText)
    data_test = pad_sequences(sequences, maxlen = MAX_SEQUENCE_LENGTH)

    #Load model for O
    K.clear_session()
    json_file_O = open('{}/{}_O_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json_O = json_file_O.read()
    json_file_O.close()
    K.clear_session()
    loaded_model_O= model_from_json(loaded_model_json_O)
    loaded_model_O.load_weights("{}/{}_O_{}.h5".format(lexicon,model,sentiment))
    score_O = round(loaded_model_O.predict(data_test).item(),3)


    #Load model for C
    K.clear_session()
    json_file_C = open('{}/{}_C_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json_C = json_file_C.read()
    json_file_C.close()
    K.clear_session()
    loaded_model_C= model_from_json(loaded_model_json_C)
    loaded_model_C.load_weights("{}/{}_C_{}.h5".format(lexicon,model,sentiment))
    score_C = round(loaded_model_C.predict(data_test).item(),3)

    #Load model for E
    K.clear_session()
    json_file_E = open('{}/{}_E_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json_E = json_file_E.read()
    json_file_E.close()
    K.clear_session()
    loaded_model_E= model_from_json(loaded_model_json_E)
    loaded_model_E.load_weights("{}/{}_E_{}.h5".format(lexicon,model,sentiment))
    score_E = round(loaded_model_E.predict(data_test).item(),3)


    #Load model for A
    K.clear_session()
    json_file_A = open('{}/{}_A_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json_A = json_file_A.read()
    json_file_A.close()
    K.clear_session()
    loaded_model_A= model_from_json(loaded_model_json_A)
    loaded_model_A.load_weights("{}/{}_A_{}.h5".format(lexicon,model,sentiment))
    score_A = round(loaded_model_A.predict(data_test).item(),3)


    #Load model for N
    K.clear_session()
    json_file_N = open('{}/{}_N_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json_N = json_file_N.read()
    json_file_N.close()
    K.clear_session()
    loaded_model_N = model_from_json(loaded_model_json_N)
    loaded_model_N.load_weights("{}/{}_N_{}.h5".format(lexicon,model,sentiment))
    score_N = round(loaded_model_N.predict(data_test).item(),3)

    print('scored')
    K.clear_session()
    return pd.Series([score_O,score_C,score_E,score_A,score_N])

#OCEAN VADER
def model_ocean_vader(normalised_text,model):
    shortText = []
    lexicon = '_vader'
    upper_threshold=0.05
    lower_threshold=-0.05
    # get aggregate scores and final sentiment
    scores = ANALYZER.polarity_scores(normalised_text)
    # get aggregate scores and final sentiment
    agg_score = scores['compound']
    pos_score = scores['pos']
    neg_score = scores['neg']
    neu_score = scores['neu']

    if agg_score > upper_threshold:
        sentiment = 'positive'
    elif agg_score < lower_threshold:
        sentiment = 'negative'
    else:
        sentiment = 'neutral'

    #sequence padding
    MAX_NB_WORDS = 20000
    MAX_SEQUENCE_LENGTH = 100

    tokenizer = Tokenizer(num_words= MAX_NB_WORDS)
    shortText.append(normalised_text)
    tokenizer.fit_on_texts(shortText)
    sequences = tokenizer.texts_to_sequences(shortText)
    data_test = pad_sequences(sequences, maxlen = MAX_SEQUENCE_LENGTH)

    #Load model for O
    json_file = open('{}/{}_O_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_O= model_from_json(loaded_model_json)
    loaded_model_O.load_weights("{}/{}_O_{}.h5".format(lexicon,model,sentiment))
    score_O = round(loaded_model_O.predict(data_test).item(),3)

    #Load model for C
    json_file = open('{}/{}_C_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_C= model_from_json(loaded_model_json)
    loaded_model_C.load_weights("{}/{}_C_{}.h5".format(lexicon,model,sentiment))
    score_C = round(loaded_model_C.predict(data_test).item(),3)

    #Load model for E
    json_file = open('{}/{}_E_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_E= model_from_json(loaded_model_json)
    loaded_model_E.load_weights("{}/{}_E_{}.h5".format(lexicon,model,sentiment))
    score_E = round(loaded_model_E.predict(data_test).item(),3)

    #Load model for A
    json_file = open('{}/{}_A_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_A= model_from_json(loaded_model_json)
    loaded_model_A.load_weights("{}/{}_A_{}.h5".format(lexicon,model,sentiment))
    score_A = round(loaded_model_A.predict(data_test).item(),3)

    #Load model for N
    json_file = open('{}/{}_N_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_N= model_from_json(loaded_model_json)
    loaded_model_N.load_weights("{}/{}_N_{}.h5".format(lexicon,model,sentiment))
    score_N = round(loaded_model_N.predict(data_test).item(),3)

    K.clear_session()
    return pd.Series([pos_score,neg_score,neu_score,agg_score,sentiment,score_O,score_C,score_E,score_A,score_N])



#OCEAN AFINN
def model_ocean_afinn(normalised_text,model):
    shortText = []
    lexicon = '_afinn'
    upper_threshold=1
    lower_threshold=-1
    afn = Afinn(emoticons=True)
    afn_score = afn.score(normalised_text)

    if afn_score >= upper_threshold:
         sentiment = 'positive'
    elif afn_score <= lower_threshold:
         sentiment = 'negative'
    else:
         sentiment = 'neutral'

    #sequence padding
    MAX_NB_WORDS = 20000
    MAX_SEQUENCE_LENGTH = 100

    tokenizer = Tokenizer(num_words= MAX_NB_WORDS)
    shortText.append(normalised_text)
    tokenizer.fit_on_texts(shortText)
    sequences = tokenizer.texts_to_sequences(shortText)
    data_test = pad_sequences(sequences, maxlen = MAX_SEQUENCE_LENGTH)

    #Load model for O
    json_file = open('{}/{}_O_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_O= model_from_json(loaded_model_json)
    loaded_model_O.load_weights("{}/{}_O_{}.h5".format(lexicon,model,sentiment))
    score_O = round(loaded_model_O.predict(data_test).item(),3)

    #Load model for C
    json_file = open('{}/{}_C_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_C= model_from_json(loaded_model_json)
    loaded_model_C.load_weights("{}/{}_C_{}.h5".format(lexicon,model,sentiment))
    score_C = round(loaded_model_C.predict(data_test).item(),3)

    #Load model for E
    json_file = open('{}/{}_E_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_E= model_from_json(loaded_model_json)
    loaded_model_E.load_weights("{}/{}_E_{}.h5".format(lexicon,model,sentiment))
    score_E = round(loaded_model_E.predict(data_test).item(),3)

    #Load model for A
    json_file = open('{}/{}_A_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_A= model_from_json(loaded_model_json)
    loaded_model_A.load_weights("{}/{}_A_{}.h5".format(lexicon,model,sentiment))
    score_A = round(loaded_model_A.predict(data_test).item(),3)

    #Load model for N
    json_file = open('{}/{}_N_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_N= model_from_json(loaded_model_json)
    loaded_model_N.load_weights("{}/{}_N_{}.h5".format(lexicon,model,sentiment))
    score_N = round(loaded_model_N.predict(data_test).item(),3)

    K.clear_session()
    return pd.Series([afn_score,sentiment,score_O,score_C,score_E,score_A,score_N])


#OCEAN SWN
def model_ocean_swn(normalised_text,model):
    shortText = []
    lexicon = '_swn'

    upper_threshold=0.01
    lower_threshold=-0.01

    nlp = spacy.load('en', parse = False, tag=False, entity=False)
    tagged_text = [(token.text, token.tag_) for token in nlp(normalised_text)]
    pos_score = neg_score = token_count = obj_score = 1

    for word, tag in tagged_text:
        ss_set = None
        if 'NN' in tag and list(swn.senti_synsets(word, 'n')):
            ss_set = list(swn.senti_synsets(word, 'n'))[0]
        elif 'VB' in tag and list(swn.senti_synsets(word, 'v')):
            ss_set = list(swn.senti_synsets(word, 'v'))[0]
        elif 'JJ' in tag and list(swn.senti_synsets(word, 'a')):
            ss_set = list(swn.senti_synsets(word, 'a'))[0]
        elif 'RB' in tag and list(swn.senti_synsets(word, 'r')):
            ss_set = list(swn.senti_synsets(word, 'r'))[0]
        # if senti-synset is found
        if ss_set:
            # add scores for all found synsets
            pos_score += ss_set.pos_score()
            neg_score += ss_set.neg_score()
            obj_score += ss_set.obj_score()
            token_count += 1

    # aggregate final scores
    final_score = pos_score - neg_score
    norm_final_score = round(float(final_score) / token_count, 2)

    if norm_final_score > upper_threshold:
         sentiment = 'positive'
    elif norm_final_score < lower_threshold:
         sentiment = 'negative'
    else:
         sentiment = 'neutral'

    #sequence padding
    MAX_NB_WORDS = 20000
    MAX_SEQUENCE_LENGTH = 100

    tokenizer = Tokenizer(num_words= MAX_NB_WORDS)
    shortText.append(normalised_text)
    tokenizer.fit_on_texts(shortText)
    sequences = tokenizer.texts_to_sequences(shortText)
    data_test = pad_sequences(sequences, maxlen = MAX_SEQUENCE_LENGTH)

    #Load model for O
    json_file = open('{}/{}_O_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_O= model_from_json(loaded_model_json)
    loaded_model_O.load_weights("{}/{}_O_{}.h5".format(lexicon,model,sentiment))
    score_O = round(loaded_model_O.predict(data_test).item(),3)

    #Load model for C
    json_file = open('{}/{}_C_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_C= model_from_json(loaded_model_json)
    loaded_model_C.load_weights("{}/{}_C_{}.h5".format(lexicon,model,sentiment))
    score_C = round(loaded_model_C.predict(data_test).item(),3)

    #Load model for E
    json_file = open('{}/{}_E_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_E= model_from_json(loaded_model_json)
    loaded_model_E.load_weights("{}/{}_E_{}.h5".format(lexicon,model,sentiment))
    score_E = round(loaded_model_E.predict(data_test).item(),3)

    #Load model for A
    json_file = open('{}/{}_A_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_A= model_from_json(loaded_model_json)
    loaded_model_A.load_weights("{}/{}_A_{}.h5".format(lexicon,model,sentiment))
    score_A = round(loaded_model_A.predict(data_test).item(),3)

    #Load model for N
    json_file = open('{}/{}_N_{}.json'.format(lexicon,model,sentiment), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_N= model_from_json(loaded_model_json)
    loaded_model_N.load_weights("{}/{}_N_{}.h5".format(lexicon,model,sentiment))
    score_N = round(loaded_model_N.predict(data_test).item(),3)

    K.clear_session()
    return pd.Series([pos_score,neg_score,obj_score,sentiment,score_O,score_C,score_E,score_A,score_N])


@app.route('/')
def main():
    return render_template('index.html')

@app.route('/showCrawler')
def showCrawler():
    return render_template('crawler.html')

@app.route('/showAnalyser')
def showAnalyser():
    return render_template('analyser.html')

@app.route('/showIdentifier')
def showIdentifier():
    return render_template('identifier.html')

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/files/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)


@app.route('/main',methods=['POST','GET'])
def predictor():
    try:
        shortText = []
        _text = request.form['inputText']
        _lexicon = request.form['inputLexicon']
        _model = request.form['inputModel']

        if _text:
            shortText.append(_text)
        else:
            value = 'error'
            text = 'Message cannot be empty.'
            return render_template('index.html',status=value,message=text)

        if _lexicon:
            lexicon = _lexicon
        else:
            value = 'error'
            text = 'Select a lexical resource.'
            return render_template('index.html',status=value,message=text)

        if _model:
            model = _model
        else:
            value = 'error'
            text = 'Select a language model learning.'
            return render_template('index.html',status=value,message=text)



        #pre-process
        normalised_text = normalisation.normalise_text(_text)

        ##VADER sentiment analysis
        if lexicon == '_vader':
            upper_threshold=0.05
            lower_threshold=-0.05
            analyzer = SentimentIntensityAnalyzer()
            scores = analyzer.polarity_scores(normalised_text)
            # get aggregate scores and final sentiment
            agg_score = scores['compound']
            pos_score = scores['pos']
            neg_score = scores['neg']
            neu_score = scores['neu']

            if agg_score > upper_threshold:
                sentiment = 'positive'
            elif agg_score < lower_threshold:
                sentiment = 'negative'
            else:
                sentiment = 'neutral'

        ##AFINN sentiment analysis
        if lexicon == '_afinn':
            upper_threshold=1
            lower_threshold=-1
            afn = Afinn(emoticons=True)
            afn_score = afn.score(normalised_text)

            if afn_score >= upper_threshold:
                 sentiment = 'positive'
            elif afn_score <= lower_threshold:
                 sentiment = 'negative'
            else:
                 sentiment = 'neutral'

        ##SWN sentiment analysis

        if lexicon == '_swn':
            upper_threshold=0.01
            lower_threshold=-0.01

            nlp = spacy.load('en', parse = False, tag=False, entity=False)
            tagged_text = [(token.text, token.tag_) for token in nlp(normalised_text)]
            pos_score = neg_score = token_count = obj_score = 1

            for word, tag in tagged_text:
                ss_set = None
                if 'NN' in tag and list(swn.senti_synsets(word, 'n')):
                    ss_set = list(swn.senti_synsets(word, 'n'))[0]
                elif 'VB' in tag and list(swn.senti_synsets(word, 'v')):
                    ss_set = list(swn.senti_synsets(word, 'v'))[0]
                elif 'JJ' in tag and list(swn.senti_synsets(word, 'a')):
                    ss_set = list(swn.senti_synsets(word, 'a'))[0]
                elif 'RB' in tag and list(swn.senti_synsets(word, 'r')):
                    ss_set = list(swn.senti_synsets(word, 'r'))[0]
                # if senti-synset is found
                if ss_set:
                    # add scores for all found synsets
                    pos_score += ss_set.pos_score()
                    neg_score += ss_set.neg_score()
                    obj_score += ss_set.obj_score()
                    token_count += 1

            # aggregate final scores
            final_score = pos_score - neg_score
            norm_final_score = round(float(final_score) / token_count, 2)

            if norm_final_score > upper_threshold:
                 sentiment = 'positive'
            elif norm_final_score < lower_threshold:
                 sentiment = 'negative'
            else:
                 sentiment = 'neutral'

        """
        check='{}/{}_O_{}'.format(lexicon,model,sentiment)
        return render_template('index.html',check=check)
        """

        #sequence padding
        MAX_NB_WORDS = 20000
        MAX_SEQUENCE_LENGTH = 100

        tokenizer = Tokenizer(num_words= MAX_NB_WORDS)
        shortText = normalisation.normalise_corpus(shortText)
        tokenizer.fit_on_texts(shortText)
        sequences = tokenizer.texts_to_sequences(shortText)
        data_test = pad_sequences(sequences, maxlen = MAX_SEQUENCE_LENGTH)


        def load_model(lexicon,model,sentiment):
            #Load model for O
            json_file = open('{}/{}_O_{}.json'.format(lexicon,model,sentiment), 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            loaded_model_O= model_from_json(loaded_model_json)
            loaded_model_O.load_weights("{}/{}_O_{}.h5".format(lexicon,model,sentiment))

            #Load model for C
            json_file = open('{}/{}_C_{}.json'.format(lexicon,model,sentiment), 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            loaded_model_C= model_from_json(loaded_model_json)
            loaded_model_C.load_weights("{}/{}_C_{}.h5".format(lexicon,model,sentiment))

            #Load model for E
            json_file = open('{}/{}_E_{}.json'.format(lexicon,model,sentiment), 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            loaded_model_E= model_from_json(loaded_model_json)
            loaded_model_E.load_weights("{}/{}_E_{}.h5".format(lexicon,model,sentiment))

            #Load model for A
            json_file = open('{}/{}_A_{}.json'.format(lexicon,model,sentiment), 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            loaded_model_A= model_from_json(loaded_model_json)
            loaded_model_A.load_weights("{}/{}_A_{}.h5".format(lexicon,model,sentiment))

            #Load model for N
            json_file = open('{}/{}_N_{}.json'.format(lexicon,model,sentiment), 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            loaded_model_N= model_from_json(loaded_model_json)
            loaded_model_N.load_weights("{}/{}_N_{}.h5".format(lexicon,model,sentiment))

            return loaded_model_O,loaded_model_C,loaded_model_E,loaded_model_A,loaded_model_N


        loaded_model_O,loaded_model_C,loaded_model_E,loaded_model_A,loaded_model_N  = load_model(lexicon,model,sentiment)

        score_O = round(loaded_model_O.predict(data_test).item(),3)
        score_C = round(loaded_model_C.predict(data_test).item(),3)
        score_E = round(loaded_model_E.predict(data_test).item(),3)
        score_A = round(loaded_model_A.predict(data_test).item(),3)
        score_N = round(loaded_model_N.predict(data_test).item(),3)

        if lexicon == '_vader':
            sentiLabels = ['Positive','Neutral','Negative']
            sentiValues = [pos_score,neu_score,neg_score]
            sentiColours = ['#79B266', '#E7E7E7', '#D2514C']

        elif lexicon == '_afinn':
            upper_threshold=1
            lower_threshold=-1
            if afn_score >= upper_threshold:
                sentiLabels = ['Overall Positive']
                sentiValues = [afn_score]
                sentiColours= ['#79B266']
            elif afn_score <= lower_threshold:
                sentiLabels = ['Overall Negative']
                sentiValues = [afn_score]
                sentiColours= ['#D2514C']
            else:
                sentiLabels = ['Overall Neutral']
                sentiValues = [afn_score]
                sentiColours= ['#E7E7E7']

        if lexicon == '_swn':
            sentiLabels = ['Positive','Objectivity','Negative']
            sentiValues = [pos_score,obj_score,neg_score]
            sentiColours = ['#79B266', '#E7E7E7', '#D2514C']

        oceanValues = [score_O,score_C,score_E,score_A,score_N]

        text = "Finished processing messages."
        value = "success"
        K.clear_session()
        return render_template('index.html',  shorttext=_text, model=_model, lexicon=_lexicon, ocean=oceanValues, sentiment=zip(sentiValues, sentiLabels), colours=sentiColours, sentiment_all=sentiment, status=value, message=text)

    except:
        text = "Unexpected error. Please try again."
        value = "error"
        K.clear_session()
        return render_template('index.html',status=value,message=text)


@app.route('/crawler',methods=['POST','GET'])
def crawler():
    try:
        _username = request.form['inputUsername']
        _query = request.form['inputQuery']
        _since = request.form['inputSince']
        _until = request.form['inputUntil']
        _maxtweets = request.form['inputMaxTweets']

        if _username or _query:
            tweet_criteria = models.TweetCriteria()
        else:
            text = "Tweet criteria cannot be empty. Please input username or query."
            value = "error"
            return render_template('crawler.html',status=value,message=text)



        if _username:
            tweet_criteria.username = _username
        elif _query:
            tweet_criteria.query = _query

        if _since:
            tweet_criteria.since = _since

        if _until:
            tweet_criteria.until = _until

        if _maxtweets:
            tweet_criteria.max_tweets = int(_maxtweets)

        now = datetime.now()
        _filename = "{}{}_{}_{}_{}".format( _username,_query,_since,_until, now.strftime("%Y%m%d%H%M%S"))

        exporter = controllers.Exporter(filename = _filename)
        miner = controllers.Scraper()

        miner.get_tweets(tweet_criteria, buffer = exporter.output_to_file)
        exporter.close()

        text = "Finished crawling data. Output file generated."
        value = "success"
        return render_template('crawler.html',status=value,message=text,filename=_filename)
    except:
        text = "Unexpected error. Please try again."
        value = "error"
        return render_template('crawler.html',status=value,message=text)


@app.route('/analyser',methods=['POST','GET'])
def analyser():
    try:
        if request.method == 'POST':
        # check if the post request has the file part
            if 'file' not in request.files:
                text = "No file part"
                value = "error"
                return render_template('analyser.html',status=value,message=text)

            _sentiment = []
            _lexicon = request.form['inputLexicon']
            _filename = ''

            file = request.files['file']

            # if user does not select file, browser also
            # submit a empty part without filename
            if file.filename == '':
                text = "No selected file"
                value = "error"
                return render_template('analyser.html',status=value,message=text)

            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                #file.save(filename)
                file.save(os.path.join(app.config['TEMP_FOLDER'], filename))
                #return redirect(url_for('uploaded_file',filename=filename))

        #precprocessing
        df = pd.read_csv("{}/{}".format(app.config['TEMP_FOLDER'], filename), delimiter=',', encoding='utf-8')
        df = df[['id', 'text', 'username']]
        df['processed_text'] = normalisation.normalise_corpus(df['text'])

        if _lexicon == '_vader':
            df[['positive','negative','neutral','compound','sentiment']] = df['processed_text'].apply(vader_sentiment)
            pos_score = round(df.loc[:,'positive'].mean(),2)
            neg_score = round(df.loc[:,'negative'].mean(),2)
            neu_score = round(df.loc[:,'neutral'].mean(),2)
            agg_score = round(df.loc[:,'compound'].mean(),2)
            sentiLabels = ['Positive','Neutral','Negative']
            sentiValues = [pos_score,neu_score,neg_score]
            sentiColours = ['#79B266', '#E7E7E7', '#D2514C']
            _filename = '{}VADER_{}'.format(app.config['UPLOAD_FOLDER'], filename)
            df.to_csv(_filename,index=False, sep=',',encoding='utf-8')


        if _lexicon == '_afinn':
            df[['score','sentiment']] = df['processed_text'].apply(afinn_sentiment)
            afn_score = round(df.loc[:,'score'].mean(),2)
            upper_threshold=1
            lower_threshold=-1
            if afn_score >= upper_threshold:
                sentiLabels = ['Overall Positive']
                sentiValues = [afn_score]
                sentiColours= ['#79B266']
            elif afn_score <= lower_threshold:
                sentiLabels = ['Overall Negative']
                sentiValues = [afn_score]
                sentiColours= ['#D2514C']
            else:
                sentiLabels = ['Overall Neutral']
                sentiValues = [afn_score]
                sentiColours= ['#E7E7E7']
            _filename = '{}AFINN_{}'.format(app.config['UPLOAD_FOLDER'], filename)
            df.to_csv(_filename,index=False, sep=',',encoding='utf-8')


        if _lexicon == '_swn':
            df[['positive','negative','objectivity','sentiment']] = df['processed_text'].apply(swn_sentiment)
            pos_score = round(df.loc[:,'positive'].mean(),2)
            neg_score = round(df.loc[:,'negative'].mean(),2)
            obj_score = round(df.loc[:,'objectivity'].mean(),2)
            sentiLabels = ['Positive','Objectivity','Negative']
            sentiValues = [pos_score,obj_score,neg_score]
            sentiColours = ['#79B266', '#E7E7E7', '#D2514C']
            _filename = '{}SWN_{}'.format(app.config['UPLOAD_FOLDER'], filename)
            df.to_csv(_filename,index=False, sep=',',encoding='utf-8')

        _sentiment = pd.read_csv(_filename)

        text = "Finished cleansing data and sentiment analysis. Output file generated."
        value = "success"
        return render_template('analyser.html',status=value,message=text,lexicon=_lexicon, table=_sentiment, sentiment=zip(sentiValues, sentiLabels), colours=sentiColours,filename=_filename)
    except:
        text = "Unexpected error. Please try again."
        value = "error"
        return render_template('analyser.html',status=value,message=text)


@app.route('/identifier',methods=['POST','GET'])
def identifier():
    try:
        if request.method == 'POST':
        # check if the post request has the file part
            if 'file' not in request.files:
                text = "No file part"
                value = "error"
                return render_template('identifier.html',status=value,message=text)

            _sentiment = []
            _lexicon = request.form['inputLexicon']
            _model = request.form['inputModel']
            _filename = ''

            file = request.files['file']

            # if user does not select file, browser also
            # submit a empty part without filename
            if file.filename == '':
                text = "No selected file"
                value = "error"
                return render_template('identifier.html',status=value,message=text)

            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['TEMP_FOLDER'], filename))


        #precprocessing
        df = pd.read_csv("{}/{}".format(app.config['TEMP_FOLDER'], filename), delimiter=',', encoding='utf-8')
        df = df[['id', 'text', 'username']]
        df['processed_text'] = normalisation.normalise_corpus(df['text'])


        if _lexicon == '_vader':
            df[['positive','negative','neutral','compound','sentiment','O','C','E','A','N']] = df['processed_text'].apply(model_ocean_vader, model = _model)
            #df[['positive','negative','neutral','compound','sentiment','processed_text']]= df['text'].apply(lexicon_vader)

            pos_score = round(df.loc[:,'positive'].mean(),2)
            neu_score = round(df.loc[:,'neutral'].mean(),2)
            neg_score = round(df.loc[:,'negative'].mean(),2)

            sentiLabels = ['Positive','Neutral','Negative']
            sentiValues = [pos_score,neu_score,neg_score]
            sentiColours = ['#79B266', '#E7E7E7', '#D2514C']

            _filename = '{}VADER_{}_{}'.format(app.config['UPLOAD_FOLDER'],_model,filename)
            df.to_csv(_filename,index=False, sep=',',encoding='utf-8')


        if _lexicon == '_afinn':
            df[['score','sentiment','O','C','E','A','N']] = df['processed_text'].apply(model_ocean_afinn, model = _model)

            afn_score = round(df.loc[:,'score'].mean(),2)

            upper_threshold=1
            lower_threshold=-1
            if afn_score >= upper_threshold:
                sentiLabels = ['Overall Positive']
                sentiValues = [afn_score]
                sentiColours= ['#79B266']
            elif afn_score <= lower_threshold:
                sentiLabels = ['Overall Negative']
                sentiValues = [afn_score]
                sentiColours= ['#D2514C']
            else:
                sentiLabels = ['Overall Neutral']
                sentiValues = [afn_score]
                sentiColours= ['#E7E7E7']
            _filename = '{}AFINN_{}_{}'.format(app.config['UPLOAD_FOLDER'],_model,filename)
            df.to_csv(_filename,index=False, sep=',',encoding='utf-8')


        if _lexicon == '_swn':
            df[['positive','negative','objectivity','sentiment','O','C','E','A','N']] = df['processed_text'].apply(model_ocean_swn, model = _model)

            pos_score = round(df.loc[:,'positive'].mean(),2)
            neg_score = round(df.loc[:,'negative'].mean(),2)
            obj_score = round(df.loc[:,'objectivity'].mean(),2)

            sentiLabels = ['Positive','Objectivity','Negative']
            sentiValues = [pos_score,obj_score,neg_score]
            sentiColours = ['#79B266', '#E7E7E7', '#D2514C']
            _filename = '{}SWN_{}_{}'.format(app.config['UPLOAD_FOLDER'],_model,filename)
            df.to_csv(_filename,index=False, sep=',',encoding='utf-8')


        score_O = round(df.loc[:,'O'].mean(),2)
        score_C = round(df.loc[:,'C'].mean(),2)
        score_E = round(df.loc[:,'E'].mean(),2)
        score_A = round(df.loc[:,'A'].mean(),2)
        score_N = round(df.loc[:,'N'].mean(),2)
        oceanValues = [score_O,score_C,score_E,score_A,score_N]

        text = "Finished personality detection. Output file generated."
        value = "success"
        K.clear_session()
        return render_template('identifier.html',status=value,message=text, model=_model, lexicon=_lexicon, ocean=oceanValues, sentiment=zip(sentiValues, sentiLabels), colours=sentiColours, filename=_filename)
    except:
        text = "Unexpected error. Please try again."
        value = "error"
        K.clear_session()
        return render_template('identifier.html',status=value,message=text)

if __name__ == '__main__':
    app.run(host=HOST,
            debug=True,  # automatic reloading enabled
            port=PORT)
