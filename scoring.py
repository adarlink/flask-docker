import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer


#Initialise VADER Sentiment
def vaderSentiment(sentence, upper_threshold=0.05, lower_threshold=-0.05):
	analyzer = SentimentIntensityAnalyzer()
	scores = analyzer.polarity_scores(sentence)
    # get aggregate scores and final sentiment
	agg_score = scores['compound']
	    
	if agg_score > upper_threshold: 
	    sentiment = 'positive'
	elif agg_score < lower_threshold:
	    sentiment = 'negative'
	else:
	    sentiment = 'neutral'

	return sentiment

def vaderCompound(sentence):
    return ANALYZER.polarity_scores(sentence)['compound']
def vaderPositive(sentence):
    return ANALYZER.polarity_scores(sentence)['pos']
def vaderNegative(sentence):
    return ANALYZER.polarity_scores(sentence)['neg']
def vaderNeutral(sentence):
    return ANALYZER.polarity_scores(sentence)['neu']